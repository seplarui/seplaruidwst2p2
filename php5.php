/*
Alumno: Sebastián Plaza Ruiz
Ejercicio: php5.php
Enunciado: Hacer un programa que muestre los números múltiplos de 3 de un array.
Ejecucion: Números múltiplos de 3.
*/


<html>
    <head>
    <title>Mostrar los múltiplos de 3 de un array</title>
    </head>
    <body>
        <?php
        
        //ARRAYS
        
        $vector=array(1,4,6,3,2,7,8,-3,-20,12,-42,32);
        
        foreach($vector as $v) {
            
            if($v%3==0) {
                echo $v.",";
            }
        }
        ?>
    </body>
</html>
